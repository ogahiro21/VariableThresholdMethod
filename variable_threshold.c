#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "var.h"
#include "bmpfile.h"

#define SIZE 256
#define LOCAL 7

void VariableThreshold(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                       Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH]);

int SearchAverage(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  int centerX,  int centerY);

int CalcDispersion(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                   int centerX, int centerY, int ave);

// binarization
void binarization(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  int threashold, int x, int y);

int main(int argc, char *argv[])
{
  imgdata idata;

  if (argc < 3) {
    printf("使用法：cpbmp コピー元.bmp コピー先.bmp\n");
  }

  else {
    if (readBMPfile(argv[1], &idata) > 0){
      printf("指定コピー元ファイル%sが見つかりません\n",argv[1]);
    }
    else {
      /* Variable threshold method */
      VariableThreshold(idata.source, idata.results);
    }
    if (writeBMPfile(argv[2], &idata) > 0){
      printf("コピー先ファイル%sに保存できませんでした\n",argv[2]);
    }
  }
}

void VariableThreshold(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                       Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH])
{
  int x,y;
  int ave, threshold, dispersion;

  for (y = 0; y < SIZE; y++) {
    for (x = 0, threshold = 0; x < SIZE; x++) {
      ave = SearchAverage(source, x, y);
      dispersion = CalcDispersion(source, x, y, ave);
      // Update threshold
      if (dispersion >= 1000) {
        threshold = ave;
      }
      binarization(source, results, threshold, x, y);
    }
  }
}

int SearchAverage(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  int centerX,  int centerY)
{
  int x,y,sum = 0, n = 0;

  for (y = -1*(LOCAL/2); y <= LOCAL/2; y++) {
    for (x = -1*(LOCAL/2); x <= LOCAL/2; x++) {
      if (centerY + y <= 255 && centerY + y > 0) {
        if (centerX + x <= 255 && centerX + x > 0) {
          n++;
          sum += source[RED][centerY+y][centerX+x];
        }
      }
    }
  }

  return sum / n;
}

int CalcDispersion(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                   int centerX, int centerY, int ave)
{
  int x,y,sigma = 0, n = 0;

  for (y = -1*(LOCAL/2); y <= LOCAL/2; y++) {
    for (x = -1*(LOCAL/2); x <= LOCAL/2; x++) {
      if (centerY + y <= 255 && centerY + y >0) {
        if (centerX + x <= 255 && centerX + x > 0) {
          n++;
          sigma += pow(source[RED][centerY+y][centerX+x]-ave, 2);
        }
      }
    }
  }

  return sigma/n;
}

void binarization(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  int threashold, int x, int y)
{
  int color;

  for (color = 0; color < 3; color++) {
    if(source[RED][y][x] >= threashold) results[color][y][x] = 255;
    else results[color][y][x] = 0;
  }
}
