# Variable Threshold Method
可変閾値処理を行う

## About Variable threshold method
- 画像背景が均一でないときに有用。  
- 画像の **局所領域の特徴量（平均値など）を閾値** とする。
- 濃度差のばらつきが小さい = ノイズなどの影響とする  
- ７ｘ７の局所領域の **分散が１０００以上** の時に閾値を更新する。
- 分散  
$`\sigma^{2} = \frac{1}{n} \sum_{j} \sum_{i} (f[i,j] - \overline{f})^{2}`$  

- 左端は白過程し、行はじめの閾値は０から始める。
- 画像領域内の画素で計算する。
- 入力画像  
![in](https://gitlab.com/ogahiro21/VariableThresholdMethod/raw/image/image/in17.jpeg)
- 出力画像  
![out](https://gitlab.com/ogahiro21/VariableThresholdMethod/raw/image/image/out17.jpeg) <br>

## Usage
**コンパイル**
```
gcc -o cpbmp variable_threshold.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in17.bmp ans17.bmp
```
